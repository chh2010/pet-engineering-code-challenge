# PET Engineering Code Challenge #

The [attached utf-8 encoded text file][care_teams] contains (mock) lists of care teams for 20,000 patients. Each line represents a list of doctors that a patient has seen, formatted as follows:

```
katie mitchell,steve edwards,melissa fleming,maurice simmmons,diane burke,debbie kuhn,tommy brooks,ella jennings
carolyn jennings,katie price,marc mills,steve edwards,brooke burke,bob flores,melissa fleming,kristina roberts
```

Write a program that takes a file or standard input, and produces a list of pairs of doctors which appear together in at least 40 different lists. For example, in the above sample, "steve edwards" and "melissa fleming" appear together twice, but every other pair appears only once. Your program should output the pair list to stdout in the same form as the input (e.g. doctor1 name,doctor2 name\n).

You MAY return an approximate solution, i.e. lists which appear at least 40 times with high probability, as long as you explain why this tradeoff improves the performance of the algorithm. Please include, either in comments or in a separate file, a brief description of the run-time and space complexity of your algorithm.

Your solution should be implemented in Javascript. Please include compilation/runtime instructions with your code. 

[care_teams]: https://bitbucket.org/chh2010/pet-engineering-code-challenge/downloads/care_teams.csv "Care Teams CSV File"